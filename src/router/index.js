import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: () => import('@/views/Home')
    },
    {
      path: '/auth/register',
      name: 'Register',
      component: () => import('@/views/auth/Register')
    },
    {
      path: '/auth/login',
      name: 'Login',
      component: () => import('@/views/auth/Login')
    }
  ]
})
